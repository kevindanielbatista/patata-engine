## Building

| Linux | Windows |
| :--: | :--: |
| [Español](Compile/build_linux_es.md) |  |
| [English](Compile/build_linux_en.md) |  |
<hr>

## Config

| [Español](Config/Config_es.md)| [English](Config/Config_en.md) |
| :--: | :--: |

<hr>
